# es_export_logs

## Intro

Usually users can export index entries with kibana, using:

* Discover > Share

But, if you do not have xPack activated, you can't export data.

I made this script to export Elasticsearch logs for a customer, the request was urgent.


## Requirements

You need to install elasticsearch client, for your elasticsearch version:

* https://elasticsearch-py.readthedocs.io/en/master/#installation


I created a virtualenv to install the client:

```
python3 -m venv venv
source venv/bin/activate
```

After that, I installed ES client for ES 6.x:

```
pip install 'elasticsearch>=6.0.0,<7.0.0'
```

## Running the script

Configure `scan_list` elements in `export.py`.

Do not forget to check `timestamp` format in your logs, to correctly configure `scan_list`.

Try with little data ranges before exporting the final complete logs, to avoid a query too expensive in time, memory or cpu.
Once you found the right query, go for it.

```
source venv/bin/activate
python export.py > output.log
```


## Links

* https://www.elastic.co/guide/en/elasticsearch/reference/6.8/query-filter-context.html
* https://elasticsearch-py.readthedocs.io/en/master/api.html#elasticsearch.Elasticsearch.search

## Example query:

```
GET /logstash-2020.06.14/_search
{
    "query": {
      "bool": {
        "must": [
          { "match" : { "kubernetes.container_name": "nginx-ingress-controller" } },
          { "match" : { "log" : "car/number/" } },
          { "match" : { "log" : "Amazon CloudFront" } }
        ],
        "filter": [
          { "range": { "@timestamp": { "gte": "2020-06-14T21:00:00.000000000+00:00", "lte": "2020-06-14T23:00:00.000000000+00:00" }}}
        ]
      }
    }
}
```
