from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
import time
es = Elasticsearch()


# provide index name and timestamp ranges
# for timestamp format, check your format in logs before
# otherwise it will not match
scan_list = [
    {
        "index_name": "logstash-2020.06.14",
        "timestamp": { "gte": "2020-06-14T00:00:00.000000000+00:00", "lte": "2020-06-14T23:59:59.000000000+00:00" },
    },
    {
        "index_name": "logstash-2020.06.15",
        "timestamp": { "gte": "2020-06-15T00:00:00.000000000+00:00", "lte": "2020-06-15T23:59:59.000000000+00:00" },
    },
    {
        "index_name": "logstash-2020.06.16",
        "timestamp": { "gte": "2020-06-16T00:00:00.000000000+00:00", "lte": "2020-06-16T23:59:59.000000000+00:00" },
    },
]


for item in scan_list:

    index_name = item.get('index_name')
    timestamp = item.get('timestamp')

    if index_name is None or timestamp is None:
        raise ValueError("Error: scan_list is not valid.")

    # prepare query
    query = {
        "query": {
            "bool": {
                "must": [
                    { "match" : { "kubernetes.container_name": "nginx-ingress-controller" } },
                    { "match" : { "log" : "search/platenumber/" } },
                    { "match" : { "log" : "Amazon CloudFront" } }
                ],
                "filter": [
                    { "range": { "@timestamp": timestamp}}
                ]
            }
        }
    }

    res = scan(
        es,
        query=query,
        scroll='2m',
        index=index_name
    )

    for hit in res:
        print(hit)

    time.sleep(30)
